// Carter Schmidt, Petr Shuller
// CS240 with Ryan Parsons
// Graph Lab

import java.util.*;

/**
 * A representation of a graph.
 * Assumes that we do not have negative cost edges in the graph.
 */
public class MyGraph implements Graph {
    // you will need some private fields to represent the graph
    // you are also likely to want some private helper methods

    private Map<Vertex, HashSet<Edge>> map;

    /**
     * Creates a MyGraph object with the given collection of vertices
     * and the given collection of edges.
     *
     * @param v a collection of the vertices in this graph
     * @param e a collection of the edges in this graph
     */
    public MyGraph(Collection<Vertex> v, Collection<Edge> e) {

        // Populate map, while checking for invalid stuff
        map = new HashMap<Vertex, HashSet<Edge>>();

        // Add vertices first
        for (Vertex vertex : v) {
            if (!map.containsKey(vertex)) {
                map.put(vertex.copyOf(), new HashSet<Edge>());
            }
        }

        // Add edges, while checking for broken things
        for (Edge edge : e) {
            // Can't have negative weights
            if (edge.getWeight() < 0) {
                throw new IllegalArgumentException("Edges must not have negative weights");
            }

            if (!map.containsKey(edge.getSource()) || !map.containsKey(edge.getDestination())) {
                throw new IllegalArgumentException("Edges's vertices don't exist");
            } else if (!map.get(edge.getSource()).add(edge.copyOf())) {
                throw new IllegalArgumentException("Can't have same directed edge");
            }
        }
    }

    /**
     * Return the collection of vertices of this graph
     *
     * @return the vertices as a collection (which is anything iterable)
     */
    public Collection<Vertex> vertices() {
        Set<Vertex> vertices = new HashSet<>();

        for (Vertex vertex : map.keySet()) {
            vertices.add(vertex.copyOf());
        }

        return vertices;
    }

    /**
     * Return the collection of edges of this graph
     *
     * @return the edges as a collection (which is anything iterable)
     */
    public Collection<Edge> edges() {
        Set<Edge> edges = new HashSet<>();

        for (HashSet<Edge> edgeSet : map.values()) {
            for (Edge edge : edgeSet) {
                edges.add(edge.copyOf());
            }
        }

        return edges;
    }

    /**
     * Return a collection of vertices adjacent to a given vertex v.
     * i.e., the set of all vertices w where edges v -> w exist in the graph.
     * Return an empty collection if there are no adjacent vertices.
     *
     * @param v one of the vertices in the graph
     * @return an iterable collection of vertices adjacent to v in the graph
     * @throws IllegalArgumentException if v does not exist.
     */
    public Collection<Vertex> adjacentVertices(Vertex v) {
        if (!map.containsKey(v)) {
            throw new IllegalArgumentException();
        }

        Set<Vertex> adjacents = new HashSet<>();

        for (Edge edge : map.get(v)) {
            adjacents.add(edge.getDestination().copyOf());
        }

        return adjacents;
    }

    /**
     * Test whether vertex b is adjacent to vertex a (i.e. a -> b) in a directed graph.
     * Assumes that we do not have negative cost edges in the graph.
     *
     * @param a one vertex
     * @param b another vertex
     * @return cost of edge if there is a directed edge from a to b in the graph,
     * return -1 otherwise.
     * @throws IllegalArgumentException if a or b do not exist.
     */
    public int edgeCost(Vertex a, Vertex b) {
        if (!map.containsKey(a) || !map.containsKey(b)) {
            throw new IllegalArgumentException();
        }

        int cost = -1;

        for (Edge edge : map.get(a)) {
            if (edge.getDestination().equals(b)) {
                cost = edge.getWeight();
                break;
            }
        }

        return cost;
    }

    public Collection<Vertex> topologicalSort() {
        ArrayList<Vertex> topoSorted = new ArrayList<Vertex>();

        Queue<Vertex> zeroQueue = new LinkedList<Vertex>();
        HashMap<Vertex, Integer> labels = new HashMap<Vertex, Integer>();

        topoSetup(zeroQueue, labels);

        // Do the topological sorting part
        while (zeroQueue.peek() != null) {
            // Grab next vertex, add it to our output
            Vertex vertex = zeroQueue.remove();
            topoSorted.add(vertex.copyOf());

            // Update neighbors
            for (Edge edge : map.get(vertex)) {
                // Decrement all neighbors inbound edge count
                labels.put(edge.getDestination(), labels.get(edge.getDestination()) - 1);

                // Add neighbor to zeroQueue if it no longer has any inbound edges
                if (labels.get(edge.getDestination()) == 0) {
                    zeroQueue.add(edge.getDestination());
                }
            }
        }

        return topoSorted;
    }

    private void topoSetup(Queue<Vertex> zeroQueue, HashMap<Vertex, Integer> labels) {
        // Initialize label thing with all vertices
        for (Vertex vertex : map.keySet()) {
            labels.put(vertex, 0);
        }

        // Label number of inbound edges for each vertex
        // Don't need to worry about mutability yet, right?
        for (HashSet<Edge> edgeSet : map.values()) {
            for (Edge edge : edgeSet) {
                labels.put(edge.getDestination(), labels.get(edge.getDestination()) + 1);
            }
        }

        // Add any vertices with no inbound edges to our zeroQueue
        for (Vertex vertex : labels.keySet()) {
            if (labels.get(vertex) == 0) {
                zeroQueue.add(vertex);
            }
        }
    }

    public Collection<Vertex> dijkstras(Vertex start, Vertex end) {
        if (!map.containsKey(start) || !map.containsKey(end)) {
            throw new IllegalArgumentException();
        }


    }


}
