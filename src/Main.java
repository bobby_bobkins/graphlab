// Carter Schmidt, Petr Shuller
// CS240 with Ryan Parsons
// Graph Lab

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        ArrayList<Vertex> vertices = new ArrayList<>();
        ArrayList<Edge> edges = new ArrayList<>();

        //Creating graph
        for (int i = 0; i < 11; i++){
            vertices.add(new Vertex(i+""));
        }

        for (int i = 1; i < 9; i++){
            edges.add(new Edge(vertices.get(i), vertices.get(i+1), i));
            edges.add(new Edge(vertices.get(i), vertices.get(i+2), i+10));

        }

        MyGraph graph = new MyGraph(vertices, edges);

        //Check Graph is correct
        System.out.println(graph.vertices().toString());

        for (Edge edge : graph.edges()) {
            for (Edge other : edges) {
                if (edge == other) {
                    //If "Bad Coder" then edges are not properly copied and are referencing the original edges arraylist
                    System.out.println("Bad coder!");
                }
            }
            System.out.println(graph.edgeCost(edge.getSource(), edge.getDestination()));
        }
        for (Vertex vertex : graph.vertices()) {
            for (Vertex other : vertices) {
                if (vertex == other) {
                    //If "Bad Coder" then vertices are not properly copied and are referencing the original vertices arraylist
                    System.out.println("Bad coder!");
                }
            }
            System.out.println(graph.adjacentVertices(vertex));
        }

        //Check exceptions
        Vertex fakeSource = new Vertex("Fake Source");
        Vertex fakeDest = new Vertex("Fake Dest");

        try {
            graph.adjacentVertices(fakeSource);
            System.err.println("Exception not thrown");
        } catch (IllegalArgumentException E){
            System.out.println("adjacentVertices exception works");
        }
        try {
            graph.edgeCost(fakeSource, fakeDest);
            System.err.println("Exception not thrown");
        } catch (IllegalArgumentException E){
            System.out.println("edgeCost exception works");
        }
        ArrayList<Edge> errorCheckEdges = new ArrayList<>();
        try {
            errorCheckEdges.add(new Edge(fakeSource, fakeDest, -1));
            MyGraph errorCheckGraph = new MyGraph(vertices, errorCheckEdges);
            System.err.println("Exception not thrown");
        } catch (IllegalArgumentException E){
            System.out.println("Negative edge exception works");
        }
        errorCheckEdges.clear();
        try {
            errorCheckEdges.add(new Edge(fakeSource, fakeDest, 1));
            MyGraph errorCheckGraph = new MyGraph(vertices, errorCheckEdges);
            System.err.println("Exception not thrown");
        } catch (IllegalArgumentException E){
            System.out.println("Non-existent vertices exception works");
        }
        errorCheckEdges.clear();
        try {
            errorCheckEdges.add(new Edge(vertices.get(0), vertices.get(1), 1));
            errorCheckEdges.add(new Edge(vertices.get(0), vertices.get(1), 1));
            MyGraph errorCheckGraphTwo = new MyGraph(vertices, errorCheckEdges);
            System.err.println("Exception not thrown");
        } catch (IllegalArgumentException E){
            System.out.println("Duplicate edge exception works");
        }
    }
}
