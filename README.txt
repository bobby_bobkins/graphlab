// Carter Schmidt, Petr Shuller
// CS240 with Ryan Parsons
// Graph Lab

1. Describe the worst-case asymptotic running times of your methods adjacentVertices and edgeCost. In your answers, use |E| for the number of edges and |V| for the number of vertices. Explain and justify your answers.
    adjacentVertices: O(|E|). Since we are using a hashmap, time to get a vertex is constant, and we then only need to copy edge of it's edges over once, which is a O(|E|) operation.
    edgeCost: O(|E|). Once again, our time to check if vertices exist is constant. We then grab one vertex, and look at each of it's edges once, which is an O(|E|) operation.
    

2. Describe how your code ensures the graph abstraction is protected. If your code makes extra copies of objects for (only) this purpose, explain why. If not, explain why it is not necessary.
    Everytime we have a public method returning vertices or edges, we make copies of those items and return only the copies. We do this so that once a collection is returned, modifying it will not modify the graph, because modifying the graph could leave it in an invalid state, for example if nodes are updated, or weights are updated.

3. Describe how you tested your code. Does your code work? Why or why not?
We populated the graph in a way we knew should work, then ran all the methods on it. We also ran a check to ensure all data in the graph is copied and not referencing the data in the arraylists used as parameters. We finally ran a list of try catch blocks to ensure all the exceptions triggered properly.